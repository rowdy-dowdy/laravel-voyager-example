-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 26, 2021 lúc 05:20 PM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `example-app`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `content`, `image`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'fdsf', NULL, NULL, NULL, 'fdsf', 1, '2021-11-19 22:03:23', '2021-11-19 22:03:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`, `slug`, `status`) VALUES
(1, 'News', NULL, NULL, '2021-11-19 02:50:06', '2021-11-19 02:50:06', 'news', 1),
(2, 'life style', NULL, NULL, '2021-11-19 22:03:10', '2021-11-19 22:03:10', 'life-style', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `collections`
--

CREATE TABLE `collections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `collections`
--

INSERT INTO `collections` (`id`, `title`, `description`, `image`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mens', NULL, 'collections\\November2021\\YfyLPCCj6FqVsYdT69Z8.jpg', 'mens', 1, '2021-11-19 20:00:00', '2021-11-26 06:06:48'),
(2, 'Womens', NULL, 'collections\\November2021\\14aSrIqnNXC589LBnhtk.jpg', 'womens', 1, '2021-11-19 21:02:00', '2021-11-26 06:06:38'),
(3, 'Kids', NULL, 'collections\\November2021\\Wa6GZ3DrU0v8xBaWnrHf.jpg', 'kids', 1, '2021-11-25 00:27:00', '2021-11-26 06:06:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `name`, `image`, `created_at`, `updated_at`, `email`, `password`, `remember_token`, `email_verified_at`) VALUES
(23, 'Việt Hùng', NULL, '2021-11-19 00:10:06', '2021-11-19 00:10:06', 'viet.hung.2898@gmail.com', '$2y$10$2CmnO9lmUwzqRS.jI7bXseUS91NTDoCNQOkbugttEx1mTyoygEMCO', 'NvHWLfuYdyLMTMhSXOPI4Q0JtMY4vJJTvRRlu21JWNiKsSZc4EWeYqJaNt1C', NULL),
(25, 'Việt Hùng', NULL, '2021-11-19 00:11:49', '2021-11-19 00:11:49', 'viet.hung.28928@gmail.com', '$2y$10$GfzVUKXsdVit789ssOowF.y6rDFHavdExefTeAkBsfA1yGBNAeV9O', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'voyager::seeders.data_rows.roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(27, 10, 'id', 'number', 'Id', 1, 0, 1, 0, 0, 0, '{}', 1),
(28, 10, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(29, 10, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(32, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(33, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(34, 10, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 2),
(35, 10, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 6),
(36, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(37, 12, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(38, 12, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(39, 12, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 5),
(40, 12, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 6),
(41, 12, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(42, 12, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":1,\"off\":0,\"checked\":true}', 7),
(43, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(44, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(45, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(46, 13, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(47, 13, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(48, 13, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 5),
(49, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(50, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(51, 13, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(52, 13, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":1,\"off\":0,\"checked\":true}', 8),
(94, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(95, 19, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(96, 19, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(97, 19, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 5),
(98, 19, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 6),
(99, 19, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(100, 19, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":1,\"off\":0,\"checked\":true}', 7),
(102, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(103, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(104, 19, 'article_hasmany_blog_relationship', 'relationship', 'blogs', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Blog\",\"table\":\"blogs\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"pivot_article_blog\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(122, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(123, 23, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(124, 23, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(125, 23, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(126, 23, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 5),
(127, 23, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":1,\"off\":0,\"checked\":true}', 6),
(128, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(129, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(130, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(131, 24, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(132, 24, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(133, 24, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 5),
(134, 24, 'images', 'multiple_images', 'Images', 0, 1, 1, 1, 1, 1, '{}', 6),
(135, 24, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(136, 24, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":1,\"off\":0,\"checked\":true}', 7),
(137, 24, 'price', 'number', 'Price', 0, 1, 1, 1, 1, 1, '{}', 8),
(138, 24, 'cost', 'number', 'Cost', 0, 1, 1, 1, 1, 1, '{}', 9),
(139, 24, 'sku', 'text', 'Sku', 0, 1, 1, 1, 1, 1, '{}', 10),
(141, 24, 'quantity', 'number', 'Quantity', 0, 1, 1, 1, 1, 1, '{}', 12),
(144, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 15),
(145, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 16),
(147, 24, 'product_belongstomany_collection_relationship', 'relationship', 'collections', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Collection\",\"table\":\"collections\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"pivot_product_collection\",\"pivot\":\"1\",\"taggable\":\"0\"}', 17),
(154, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(155, 26, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(156, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(157, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(158, 27, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(159, 27, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(160, 27, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(161, 27, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(162, 27, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(163, 27, 'gallery_id', 'text', 'Gallery Id', 0, 1, 1, 1, 1, 1, '{}', 8),
(164, 27, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":1,\"off\":0,\"checked\":true}', 9),
(165, 27, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(166, 27, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(167, 27, 'photo_belongsto_gallery_relationship', 'relationship', 'galleries', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Gallery\",\"table\":\"galleries\",\"type\":\"belongsTo\",\"column\":\"gallery_id\",\"key\":\"id\",\"label\":\"slug\",\"pivot_table\":\"articles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(168, 26, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(169, 27, 'location_title', 'text', 'Location Title', 0, 1, 1, 1, 1, 1, '{}', 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(10, 'customers', 'customers', 'Customer', 'Customers', 'voyager-people', 'App\\Models\\Customer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-16 02:33:41', '2021-11-16 02:47:40'),
(12, 'pages', 'pages', 'Page', 'Pages', 'voyager-window-list', 'App\\Models\\Page', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-19 02:28:41', '2021-11-19 02:28:41'),
(13, 'blogs', 'blogs', 'Blog', 'Blogs', 'voyager-news', 'App\\Models\\Blog', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-19 02:36:33', '2021-11-19 02:44:48'),
(19, 'articles', 'articles', 'Article', 'Articles', 'voyager-file-text', 'App\\Models\\Article', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-19 19:09:41', '2021-11-19 22:03:43'),
(23, 'collections', 'collections', 'Collection', 'Collections', NULL, 'App\\Models\\Collection', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-19 19:58:39', '2021-11-19 20:52:05'),
(24, 'products', 'products', 'Product', 'Products', NULL, 'App\\Models\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-19 20:37:27', '2021-11-21 18:54:28'),
(26, 'galleries', 'galleries', 'Gallery', 'Galleries', NULL, 'App\\Models\\Gallery', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-19 23:50:00', '2021-11-24 18:19:55'),
(27, 'photos', 'photos', 'Photo', 'Photos', NULL, 'App\\Models\\Photo', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-19 23:51:14', '2021-11-26 06:37:35');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `galleries`
--

INSERT INTO `galleries` (`id`, `title`, `created_at`, `updated_at`, `slug`) VALUES
(1, 'main top', '2021-11-19 23:52:00', '2021-11-24 18:20:45', 'main-top');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(2, 'header menu', '2021-11-20 03:19:36', '2021-11-20 03:19:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-11-15 02:50:13', '2021-11-15 02:50:13', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 6, '2021-11-15 02:50:13', '2021-11-20 00:06:57', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 31, 2, '2021-11-15 02:50:13', '2021-11-20 00:07:06', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 31, 3, '2021-11-15 02:50:13', '2021-11-20 00:07:06', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2021-11-15 02:50:13', '2021-11-20 00:07:10', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2021-11-15 02:50:13', '2021-11-20 00:07:02', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2021-11-15 02:50:13', '2021-11-20 00:07:02', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2021-11-15 02:50:13', '2021-11-20 00:07:02', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2021-11-15 02:50:13', '2021-11-20 00:07:02', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2021-11-15 02:50:13', '2021-11-20 00:07:10', 'voyager.settings.index', NULL),
(12, 1, 'Customers', '', '_self', 'voyager-people', '#000000', 31, 1, '2021-11-16 02:33:41', '2021-11-20 00:07:06', 'voyager.customers.index', 'null'),
(13, 1, 'Pages', '', '_self', 'voyager-window-list', NULL, NULL, 2, '2021-11-19 02:28:41', '2021-11-19 23:58:27', 'voyager.pages.index', NULL),
(14, 1, 'Blogs', '', '_self', 'voyager-news', '#000000', 29, 1, '2021-11-19 02:36:33', '2021-11-19 23:59:32', 'voyager.blogs.index', 'null'),
(19, 1, 'Articles', '', '_self', 'voyager-file-text', NULL, 29, 2, '2021-11-19 19:09:41', '2021-11-19 23:59:36', 'voyager.articles.index', NULL),
(22, 1, 'Collections', '', '_self', 'voyager-umbrella', '#000000', 30, 1, '2021-11-19 19:58:39', '2021-11-20 00:05:27', 'voyager.collections.index', 'null'),
(23, 1, 'Products', '', '_self', 'voyager-droplet', '#000000', 30, 2, '2021-11-19 20:37:27', '2021-11-20 00:05:35', 'voyager.products.index', 'null'),
(26, 1, 'Galleries', '', '_self', 'voyager-images', '#000000', 28, 1, '2021-11-19 23:50:00', '2021-11-19 23:58:12', 'voyager.galleries.index', 'null'),
(27, 1, 'Photos', '', '_self', 'voyager-photo', '#000000', 28, 2, '2021-11-19 23:51:14', '2021-11-19 23:58:14', 'voyager.photos.index', 'null'),
(28, 1, 'Library', '', '_self', 'voyager-shop', '#000000', NULL, 5, '2021-11-19 23:58:00', '2021-11-20 00:01:22', NULL, ''),
(29, 1, 'Blog Posts', '', '_self', 'voyager-documentation', '#000000', NULL, 3, '2021-11-19 23:59:20', '2021-11-20 00:03:14', NULL, ''),
(30, 1, 'Products', '', '_self', 'voyager-tag', '#000000', NULL, 4, '2021-11-20 00:01:03', '2021-11-20 00:01:22', NULL, ''),
(31, 1, 'Users', '', '_self', 'voyager-group', '#000000', NULL, 7, '2021-11-20 00:06:49', '2021-11-20 00:07:10', NULL, ''),
(32, 2, 'Home', '/', '_self', NULL, '#000000', NULL, 1, '2021-11-20 03:19:58', '2021-11-22 19:17:03', NULL, ''),
(33, 2, 'Categories', '', '_self', NULL, '#000000', NULL, 3, '2021-11-20 03:20:12', '2021-11-22 19:17:42', NULL, ''),
(35, 2, 'Shop', '', '_self', NULL, '#000000', NULL, 4, '2021-11-22 19:16:21', '2021-11-22 19:17:42', NULL, ''),
(36, 2, 'About', '', '_self', NULL, '#000000', NULL, 5, '2021-11-22 19:16:29', '2021-11-22 19:17:42', NULL, ''),
(37, 2, 'Contact', '', '_self', NULL, '#000000', NULL, 6, '2021-11-22 19:16:35', '2021-11-22 19:17:42', NULL, ''),
(38, 2, 'Category 1', '', '_self', NULL, '#000000', 33, 1, '2021-11-22 19:16:42', '2021-11-22 19:17:12', NULL, ''),
(39, 2, 'Category 2', '', '_self', NULL, '#000000', 33, 2, '2021-11-22 19:16:48', '2021-11-22 19:17:15', NULL, ''),
(40, 2, 'Product 1', '', '_self', NULL, '#000000', 35, 1, '2021-11-22 19:16:54', '2021-11-22 19:17:18', NULL, ''),
(41, 2, 'Product 2', '', '_self', NULL, '#000000', 35, 2, '2021-11-22 19:17:00', '2021-11-22 19:17:20', NULL, ''),
(42, 2, 'News', '', '_self', NULL, '#000000', NULL, 2, '2021-11-22 19:17:38', '2021-11-22 19:17:42', NULL, ''),
(43, 2, 'product 1', '', '_self', NULL, '#000000', 38, 1, '2021-11-23 02:05:50', '2021-11-23 02:06:00', NULL, ''),
(44, 2, 'product 2', '', '_self', NULL, '#000000', 38, 2, '2021-11-23 02:05:55', '2021-11-23 02:06:03', NULL, ''),
(45, 2, 'post 1', '', '_self', NULL, '#000000', 42, 1, '2021-11-23 02:06:18', '2021-11-23 02:06:36', NULL, ''),
(46, 2, 'post 2', '', '_self', NULL, '#000000', 42, 2, '2021-11-23 02:06:23', '2021-11-23 02:06:40', NULL, ''),
(47, 2, 'post 11', '', '_self', NULL, '#000000', 45, 1, '2021-11-23 02:06:28', '2021-11-23 02:06:31', NULL, '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2016_01_01_000000_add_voyager_user_fields', 2),
(6, '2016_01_01_000000_create_data_types_table', 2),
(7, '2016_05_19_173453_create_menu_table', 2),
(8, '2016_10_21_190000_create_roles_table', 2),
(9, '2016_10_21_190000_create_settings_table', 2),
(10, '2016_11_30_135954_create_permission_table', 2),
(11, '2016_11_30_141208_create_permission_role_table', 2),
(12, '2016_12_26_201236_data_types__add__server_side', 2),
(13, '2017_01_13_000000_add_route_to_menu_items_table', 2),
(14, '2017_01_14_005015_create_translations_table', 2),
(15, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 2),
(16, '2017_03_06_000000_add_controller_to_data_types_table', 2),
(17, '2017_04_21_000000_add_order_to_data_rows_table', 2),
(18, '2017_07_05_210000_add_policyname_to_data_types_table', 2),
(19, '2017_08_05_000000_add_group_to_settings_table', 2),
(20, '2017_11_26_013050_add_user_role_relationship', 2),
(21, '2017_11_26_015000_create_user_roles_table', 2),
(22, '2018_03_11_000000_add_user_settings', 2),
(23, '2018_03_14_000000_add_details_to_data_types_table', 2),
(24, '2018_03_16_000000_make_settings_value_nullable', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(2, 'browse_bread', NULL, '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(3, 'browse_database', NULL, '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(4, 'browse_media', NULL, '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(5, 'browse_compass', NULL, '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(6, 'browse_menus', 'menus', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(7, 'read_menus', 'menus', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(8, 'edit_menus', 'menus', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(9, 'add_menus', 'menus', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(10, 'delete_menus', 'menus', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(11, 'browse_roles', 'roles', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(12, 'read_roles', 'roles', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(13, 'edit_roles', 'roles', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(14, 'add_roles', 'roles', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(15, 'delete_roles', 'roles', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(16, 'browse_users', 'users', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(17, 'read_users', 'users', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(18, 'edit_users', 'users', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(19, 'add_users', 'users', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(20, 'delete_users', 'users', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(21, 'browse_settings', 'settings', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(22, 'read_settings', 'settings', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(23, 'edit_settings', 'settings', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(24, 'add_settings', 'settings', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(25, 'delete_settings', 'settings', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(31, 'browse_customers', 'customers', '2021-11-16 02:33:41', '2021-11-16 02:33:41'),
(32, 'read_customers', 'customers', '2021-11-16 02:33:41', '2021-11-16 02:33:41'),
(33, 'edit_customers', 'customers', '2021-11-16 02:33:41', '2021-11-16 02:33:41'),
(34, 'add_customers', 'customers', '2021-11-16 02:33:41', '2021-11-16 02:33:41'),
(35, 'delete_customers', 'customers', '2021-11-16 02:33:41', '2021-11-16 02:33:41'),
(36, 'browse_pages', 'pages', '2021-11-19 02:28:41', '2021-11-19 02:28:41'),
(37, 'read_pages', 'pages', '2021-11-19 02:28:41', '2021-11-19 02:28:41'),
(38, 'edit_pages', 'pages', '2021-11-19 02:28:41', '2021-11-19 02:28:41'),
(39, 'add_pages', 'pages', '2021-11-19 02:28:41', '2021-11-19 02:28:41'),
(40, 'delete_pages', 'pages', '2021-11-19 02:28:41', '2021-11-19 02:28:41'),
(41, 'browse_blogs', 'blogs', '2021-11-19 02:36:33', '2021-11-19 02:36:33'),
(42, 'read_blogs', 'blogs', '2021-11-19 02:36:33', '2021-11-19 02:36:33'),
(43, 'edit_blogs', 'blogs', '2021-11-19 02:36:33', '2021-11-19 02:36:33'),
(44, 'add_blogs', 'blogs', '2021-11-19 02:36:33', '2021-11-19 02:36:33'),
(45, 'delete_blogs', 'blogs', '2021-11-19 02:36:33', '2021-11-19 02:36:33'),
(66, 'browse_articles', 'articles', '2021-11-19 19:09:41', '2021-11-19 19:09:41'),
(67, 'read_articles', 'articles', '2021-11-19 19:09:41', '2021-11-19 19:09:41'),
(68, 'edit_articles', 'articles', '2021-11-19 19:09:41', '2021-11-19 19:09:41'),
(69, 'add_articles', 'articles', '2021-11-19 19:09:41', '2021-11-19 19:09:41'),
(70, 'delete_articles', 'articles', '2021-11-19 19:09:41', '2021-11-19 19:09:41'),
(81, 'browse_collections', 'collections', '2021-11-19 19:58:39', '2021-11-19 19:58:39'),
(82, 'read_collections', 'collections', '2021-11-19 19:58:39', '2021-11-19 19:58:39'),
(83, 'edit_collections', 'collections', '2021-11-19 19:58:39', '2021-11-19 19:58:39'),
(84, 'add_collections', 'collections', '2021-11-19 19:58:39', '2021-11-19 19:58:39'),
(85, 'delete_collections', 'collections', '2021-11-19 19:58:39', '2021-11-19 19:58:39'),
(86, 'browse_products', 'products', '2021-11-19 20:37:27', '2021-11-19 20:37:27'),
(87, 'read_products', 'products', '2021-11-19 20:37:27', '2021-11-19 20:37:27'),
(88, 'edit_products', 'products', '2021-11-19 20:37:27', '2021-11-19 20:37:27'),
(89, 'add_products', 'products', '2021-11-19 20:37:27', '2021-11-19 20:37:27'),
(90, 'delete_products', 'products', '2021-11-19 20:37:27', '2021-11-19 20:37:27'),
(96, 'browse_galleries', 'galleries', '2021-11-19 23:50:00', '2021-11-19 23:50:00'),
(97, 'read_galleries', 'galleries', '2021-11-19 23:50:00', '2021-11-19 23:50:00'),
(98, 'edit_galleries', 'galleries', '2021-11-19 23:50:00', '2021-11-19 23:50:00'),
(99, 'add_galleries', 'galleries', '2021-11-19 23:50:00', '2021-11-19 23:50:00'),
(100, 'delete_galleries', 'galleries', '2021-11-19 23:50:00', '2021-11-19 23:50:00'),
(101, 'browse_photos', 'photos', '2021-11-19 23:51:14', '2021-11-19 23:51:14'),
(102, 'read_photos', 'photos', '2021-11-19 23:51:14', '2021-11-19 23:51:14'),
(103, 'edit_photos', 'photos', '2021-11-19 23:51:14', '2021-11-19 23:51:14'),
(104, 'add_photos', 'photos', '2021-11-19 23:51:14', '2021-11-19 23:51:14'),
(105, 'delete_photos', 'photos', '2021-11-19 23:51:14', '2021-11-19 23:51:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location_title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `photos`
--

INSERT INTO `photos` (`id`, `title`, `description`, `image`, `link`, `gallery_id`, `status`, `created_at`, `updated_at`, `location_title`) VALUES
(1, 'banner 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt corrupti consequatur, vitae voluptatibus aliquam repellendus aliquid aspernatur sint', 'photos\\November2021\\0GNswHkfLI0zSgWBNa3b.jpg', NULL, 1, 1, '2021-11-24 03:36:00', '2021-11-26 07:05:54', 'right'),
(2, 'banner 2', NULL, 'photos\\November2021\\ivzpfg6uM0JBzMSsilwh.jpg', NULL, 1, 1, '2021-11-24 03:36:00', '2021-11-26 07:06:03', 'left'),
(3, 'banner 3', NULL, 'photos\\November2021\\V7XRc5n7wzYCphenkZO9.jpg', NULL, 1, 1, '2021-11-24 03:36:00', '2021-11-26 06:33:56', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pivot_article_blog`
--

CREATE TABLE `pivot_article_blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pivot_article_blog`
--

INSERT INTO `pivot_article_blog` (`id`, `blog_id`, `article_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pivot_product_collection`
--

CREATE TABLE `pivot_product_collection` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pivot_product_collection`
--

INSERT INTO `pivot_product_collection` (`id`, `product_id`, `collection_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, NULL, NULL),
(2, 3, 2, NULL, NULL),
(3, 3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `price` decimal(10,0) DEFAULT NULL,
  `cost` decimal(10,0) DEFAULT NULL,
  `sku` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `options` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`options`)),
  `variants` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`variants`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `content`, `images`, `slug`, `status`, `price`, `cost`, `sku`, `quantity`, `created_at`, `updated_at`, `options`, `variants`) VALUES
(2, 'fsadf', NULL, NULL, NULL, 'fsadf', 1, NULL, NULL, NULL, NULL, '2021-11-19 21:02:30', '2021-11-19 21:02:30', NULL, NULL),
(3, 'fasdfasd', NULL, NULL, '[\"products\\\\November2021\\\\V5I2dzgFQy90AQ2Hnj4K.png\",\"products\\\\November2021\\\\IK78DhrrH5hhndDzcsJq.png\",\"products\\\\November2021\\\\FKa7xaUPoKIEMurrIFk2.png\"]', 'fasdfasd', 1, NULL, NULL, NULL, NULL, '2021-11-19 21:03:00', '2021-11-21 18:55:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-11-15 02:50:13', '2021-11-15 02:50:13'),
(2, 'user', 'Normal User', '2021-11-15 02:50:13', '2021-11-15 02:50:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Việt Hùng', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\November2021\\vggxXCEKyDgmWXfvJxX1.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(14, 'site.color', 'Color', 'amber', '{\r\n    \"default\" : \"blue\",\r\n    \"options\" : {\r\n        \"blue\": \"Blue\",\r\n        \"amber\": \"Amber\",\r\n        \"purple\": \"Purple\",\r\n        \"violet\": \"Violet\",\r\n        \"rose\": \"Rose\",\r\n        \"fuchsia\": \"Fuchsia\",\r\n        \"lime\": \"Lime\",\r\n        \"green\": \"Green\",\r\n        \"gray\": \"Gray\"\r\n    }\r\n}', 'select_dropdown', 6, 'Site');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Việt Hùng', 'viet.hung.2898@gmail.com', 'users/default.png', NULL, '$2y$10$F34MnmFIYGs0oCV5qNOyNuGvTJMPQCG6mM7yew6I5rrze1iFgTRL2', NULL, NULL, '2021-11-15 02:21:47', '2021-11-15 02:21:47'),
(2, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$CcYyIIAIdKgyVxSokmuryOBknbkpYgozFRGXOZXaBsBBrZWlCqFLC', '2P7ColzYrK3neqoM6qIkSIE4HjgraPqAMMkdZyRVhoFFWKMxDkXzManWIvVy', '{\"locale\":\"en\"}', '2021-11-15 02:50:46', '2021-11-19 02:45:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_status_index` (`status`);

--
-- Chỉ mục cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_status_index` (`status`);

--
-- Chỉ mục cho bảng `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `collections_slug_unique` (`slug`),
  ADD KEY `collections_status_index` (`status`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Chỉ mục cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Chỉ mục cho bảng `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `galleries_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Chỉ mục cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`),
  ADD KEY `pages_status_index` (`status`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Chỉ mục cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photos_status_index` (`status`);

--
-- Chỉ mục cho bảng `pivot_article_blog`
--
ALTER TABLE `pivot_article_blog`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `pivot_product_collection`
--
ALTER TABLE `pivot_product_collection`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_status_index` (`status`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Chỉ mục cho bảng `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `collections`
--
ALTER TABLE `collections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT cho bảng `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `pivot_article_blog`
--
ALTER TABLE `pivot_article_blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `pivot_product_collection`
--
ALTER TABLE `pivot_product_collection`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Các ràng buộc cho bảng `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

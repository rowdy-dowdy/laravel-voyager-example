const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
const origin   = window.location.origin;

const getCollection = async () => {
  try {

    const response = await fetch(origin + '/api/collection', {
      method: 'GET',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-TOKEN': csrfToken
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      // body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error('Something wrong...')
    }
    return await response.json();

  } catch (error) {
    console.log(error);
  }
}

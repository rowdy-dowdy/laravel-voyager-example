var jsOpen = document.querySelectorAll('.js-open')

function renderList(el, data) {

  for(let i = 0; i < data.length; i++) {
    if (data[i].childrens.length > 0) {
      let child = renderList(el, data[i].childrens)
      el += `<li><a href="#" data-id="${data[i].id}" class="nav-link js-nav-link">${data[i].title}</a><ul>${child}</ul></li>`
    } else {
      el += `<li><a href="#" data-id="${data[i].id}" class="nav-link js-nav-link">${data[i].title}</a></li>`
    }
  }
  return el;
}

if (jsOpen) {
  jsOpen.forEach(v => {
    v.addEventListener('click', async (e) => {
      e.preventDefault()

      let dropdown = e.target.nextElementSibling

      if ( dropdown.dataset.open == 'false' ) {
        var rect = v.getBoundingClientRect();
        console.log(rect.top, rect.right, rect.bottom, rect.left);

        // console.log(position);

        dropdown.classList.add('show')
        dropdown.dataset.open = 'true'

        let listDOM = ''

        listDOM += `<li><span class="nav-link nav-link-dis">Search ...</span></li>`
        dropdown.innerHTML = listDOM

        listDOM = ''

        try {
          let data = await getCollection()

          listDOM = renderList(listDOM, data)
        }
        catch (err) {
          console.log(err);
        }

        dropdown.innerHTML = listDOM

      } else {
        dropdown.classList.remove('show')
        dropdown.dataset.open = 'false'

        console.log(2);
      }
    })
  });
}

<style>
  .js-botbar {
    pointer-events: none;
  }
  .js-botbar.js-nav-mobile-show {
    pointer-events: initial;
  }

  .js-botbar .js-bg {
    opacity: 0;
    transition: all .3s ease-in-out;
  }
  .js-botbar.js-nav-mobile-show .js-bg {
    opacity: 1;
  }

  .js-botbar .js-content {
    transform: translateX(-100%);
    transition: all .3s ease-in-out;
  }
  .js-botbar.js-nav-mobile-show .js-content {
    transform: translateX(0);
  }

  @media (min-width: 768px) {
    .js-botbar {
      pointer-events: initial;
    }
    .js-botbar .js-content {
      transform: translateX(0);
    }
  }
</style>

<header class="flex-none bg-white z-10 relative">
  <div class="topbar border-b py-2 text-gray-600">
    <x-container class="w-full max-w-7xl mx-auto flex flex-wrap text-sm">
      <div class="w-full md:w-1/2 flex justify-center md:justify-start space-x-4">
        <a href="#" class="inline-flex items-center space-x-2">
          <i class='bx bxs-phone-call' ></i>
          + 0399633237
        </a>
        <span class="inline-flex w-[1px] bg-current"></span>
        <a href="#" class="inline-flex items-center space-x-2">
          <i class='bx bxs-envelope' ></i>
          + 0399633237
        </a>
      </div>
      <div class="w-full md:w-1/2 text-center md:text-right">
        <h3 class="">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h3>
      </div>
    </x-container>
  </div>

  <div class="mainbar border-b">
    <x-container class="flex items-center py-2">
      <div class="flex-1 block md:hidden text-2xl">
        <a href="#" class="js-open-menu inline-flex p-2 rounded hover:bg-primary-100 text-current"><i class='bx bx-menu' ></i></a>
      </div>

      <div class="flex-1">
        @if (setting('site.logo'))
          <img src="{{ Voyager::image(setting('site.logo')) }}" width="100" alt="{{setting('site.title') ?? '' }}" loading="lazy" class="block mx-auto md:ml-0">
        @else
          <h1 class="text-center md:text-left">{{setting('site.title') ?? '' }}</h1>
        @endif
      </div>

      <div class="flex-1 hidden md:flex justify-center">
        <div class="w-full max-w-sm rounded-full border border-gray-300 text-gray-600 focus-within:border-primary-600">
          <form action="" method="get" class="flex w-full">
            @csrf
            <input type="text" name="q" class="flex-grow min-w-0 peer focus:outline-none border-0 focus:ring-0 px-4 py-2 bg-transparent">
            <span class="inline-flex w-[1px] my-2 bg-current peer-focus:bg-primary-600"></span>
            <button type="submit" class="flex-none px-4 py-2 hover:bg-primary text-sm">
              {{ __('Seach') }}
            </button>
          </form>
        </div>
      </div>

      <div class="flex-1 flex justify-end items-center text-right text-2xl">
        <a href="#" class="hidden md:inline-flex p-2 rounded hover:bg-primary-100 text-current">
          <i class='bx bxs-user'></i>
        </a>
        <span class="hidden md:inline-flex w-[1px] h-4 bg-current mx-2"></span>
        <a href="#" class="inline-flex p-2 rounded hover:bg-primary-100 text-current">
          <i class='bx bxs-cart' ></i>
        </a>
      </div>
    </x-container>
  </div>

  <div class="js-botbar border-b fixed top-0 left-0 right-0 bottom-0 md:!block md:relative overflow-hidden md:overflow-visible">
    <div class="js-bg absolute w-full h-full top-0 left-0 bg-black/70"></div>
    <div class="js-content relative w-full h-full max-w-xs md:w-inherit md:max-w-inherit bg-white shadow md:shadow-none overflow-auto md:overflow-visible">
      <div class="logo md:hidden px-4 py-4 flex justify-between items-center border-b">
        @if (setting('site.logo'))
          <img src="{{ Voyager::image(setting('site.logo')) }}" width="50" alt="{{setting('site.title') ?? '' }}" loading="lazy">
        @else
          <h1 class="text-center md:text-left">{{setting('site.title') ?? '' }}</h1>
        @endif

        <span class="js-close-menu inline-flex p-2 rounded hover:bg-primary-100 text-2xl cursor-pointer">
          <i class='bx bx-x' ></i>
        </span>
      </div>

      <x-container class="flex md:justify-center py-2">
        {{ menu('header menu','layouts.custom_menu') }}
      </x-container>
    </div>
  </div>
</header>

<script>
  var openMenu = document.querySelector('.js-open-menu')
  var botbarHeader = document.querySelector('.js-botbar')
  var bgBotbarHeader = botbarHeader.querySelector('.js-bg')
  var closeMenu = botbarHeader.querySelector('.js-close-menu')


  function clickToggleMenu (el) {
    // el.classList.toggle('js-nav-mobile-show')
    if (!el.classList.contains('js-nav-mobile-show')) {
      el.classList.add('js-nav-mobile-show')

      document.body.style.top = `-${document.documentElement.scrollTop}px`;
      document.body.style.position = 'fixed';
    } else {
      el.classList.remove('js-nav-mobile-show')

      const scrollY = document.body.style.top;
      document.body.style.position = '';
      document.body.style.top = '';
      window.scrollTo(0, parseInt(scrollY || '0') * -1);
    }
  }

  var bindClickToggleMenu;

  if (openMenu && botbarHeader && closeMenu && bgBotbarHeader) {
    bindClickToggleMenu = clickToggleMenu.bind(event,botbarHeader)

    openMenu.addEventListener('click', bindClickToggleMenu)
    closeMenu.addEventListener('click', bindClickToggleMenu)
    bgBotbarHeader.addEventListener('click', bindClickToggleMenu)
  }

  document.body.addEventListener('scroll', (e) => {
    console.log(e);
  })
</script>

<style>
  .menu li:hover > .dropdown {
    visibility: visible;
    opacity: 100;
  }

  @media (min-width: 768px) {
    .menu > li:hover > .dropdown + .nav-link .open-drop {
      transform: rotate(-180deg) !important;
    }
    .menu > li > .dropdown > li:hover > .dropdown + .nav-link .open-drop {
      transform: rotate(-90deg) !important;
    }
  }
</style>

<ul class="menu dropdown w-full md:w-initial flex flex-col md:flex-row items-start md:items-center md:space-x-4" data-parent="0">
  @foreach($items as $link)
    @php
      $path = request()->path();
    @endphp

    <li class="relative group w-full md:w-initial flex flex-col">
      @if (count($link->children) > 0)
        <ul
          data-parent="1"
          class="dropdown w-full flex flex-col rounded h-0 md:!h-initial md:absolute top-[calc(100%+.5rem)] -left-4 md:invisible md:opacity-0 transition-all duration-300 ease-in-out md:w-max md:min-w-[230px] bg-white md:shadow md:border border-gray-100 overflow-hidden md:overflow-visible px-2"
          data-open="false" >
          @foreach ($link->children as $child_link)
            <li class="first-of-type:mt-2 last-of-type:mb-2 relative group w-full md:w-initial flex flex-col">
              @if (count($child_link->children) > 0)
                <ul
                  data-parent="2"
                  class="dropdown w-full flex flex-col rounded h-0 md:!h-initial md:absolute top-0 left-[calc(100%+1rem)] md:invisible md:opacity-0 transition-all duration-300 ease-in-out md:w-max md:min-w-[230px] bg-white md:shadow md:border border-gray-100 overflow-hidden md:overflow-visible px-2"
                  data-open="false" >
                  <span class="absolute w-4 h-full top-0 right-full"></span>

                  @foreach ($child_link->children as $grand_link)
                    <li class="first-of-type:mt-2 last-of-type:mb-2 w-full md:w-initial">
                      <a href="#" data-parent="2" class="nav-link w-full block px-2 py-2 hover:bg-primary-100">
                        {{ $grand_link->title }}
                      </a>
                    </li>
                  @endforeach
                </ul>
              @endif

              <a href="#" data-parent="1" class="nav-link order-first w-full inline-flex items-center justify-between space-x-4 px-2 py-2 hover:bg-primary-100">
                <span>{{ $child_link->title }}</span>

                @if (count($child_link->children) > 0)
                  <span class="open-drop inline-flex transition-all duration-300 ease-in-out p-2">
                    <i class='bx bx-chevron-down md:!-rotate-90'></i>
                  </span>
                @endif
              </a>
            </li>
          @endforeach
        </ul>
      @endif

      <a href="#" data-parent="0" class="nav-link order-first w-full inline-flex items-center justify-between space-x-2 px-2 py-2 md:border-b-2 border-transparent md:rounded-t hover:bg-primary-100 @if($path == $link->link()) border-primary-500 @endif">
        <span>{{ $link->title }}</span>

        @if (count($link->children) > 0)
          <span class="open-drop inline-flex transition-all duration-300 ease-in-out p-2">
            <i class='bx bx-chevron-down'></i>
          </span>
        @endif
      </a>
    </li>

  @endforeach

  <li class="relative group w-full md:hidden">
    <a href="#" data-parent="0" class="nav-link order-first w-full inline-flex items-center space-x-2 px-2 py-2 md:border-b-2 border-transparent md:rounded-t hover:bg-primary-100">
      <span>
        <i class='bx bxs-user'></i>
      </span>
      <span>{{ __("Login") }}</span>
    </a>
  </li>

  <li class="relative group w-full md:hidden">
    <a href="#" data-parent="0" class="nav-link order-first w-full inline-flex items-center space-x-2 px-2 py-2 md:border-b-2 border-transparent md:rounded-t hover:bg-primary-100">
      <span class="text-xl">
        <i class='bx bxs-user-plus'></i>
      </span>
      <span>{{ __("Register") }}</span>
    </a>
  </li>
</ul>

<script>
  var navLink = document.querySelectorAll('.nav-link')

  function dropdownMenu (el,index) {
    event.preventDefault()
    event.stopPropagation()

    let dropdown = el.previousElementSibling
    // console.log(dropdown);
    if (dropdown) {
      if (dropdown.dataset.open == 'false') {
        dropdown.dataset.open = true
        dropdown.style.height = dropdown.scrollHeight + 'px'
        el.querySelector('.open-drop').style.transform = 'rotate(-180deg)'

        setTimeout(() => {
          dropdown.style.height = 'initial'
        }, 300);
      } else {
        dropdown.dataset.open = false
        dropdown.style.height = dropdown.offsetHeight+ 'px'
        el.querySelector('.open-drop').style.transform = 'rotate(0deg)'

        setTimeout(() => {
          dropdown.style.height = '0px'
        }, 0);
      }
    }

    // close dropdown
    navLink.forEach((v,i) => {
      // console.log(i, index);
      if (v != el && (v.dataset.parent == el.dataset.parent || parseInt(v.dataset.parent) > parseInt(el.dataset.parent))) {
        let dropdown = v.previousElementSibling
        if (!dropdown) return

        dropdown.dataset.open = false
        dropdown.style.height = dropdown.offsetHeight+ 'px'
        v.querySelector('.open-drop').style.transform = 'rotate(0deg)'

        setTimeout(() => {
          dropdown.style.height = '0px'
        }, 0);
      }

    });

    // console.log(listNavLink);
  }

  var bindEventDropdown;

  navLink.forEach((v,i) => {
    //
    bindEventDropdown = dropdownMenu.bind(event,v,i)

    let openDrop = v.querySelector('.open-drop')
    if (openDrop)
      openDrop.addEventListener('click', bindEventDropdown)
  });
</script>

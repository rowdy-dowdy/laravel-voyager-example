@if (Auth::guard('user')->check())
  @php
    $user = Auth::guard('user')->user();
  @endphp

  <nav class="bg-primary-600 text-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="px-4 sm:px-6 lg:px-8">
      <div class="flex justify-between h-12">
        <div class="flex">
          <!-- Logo -->
          <div class="flex-shrink-0 flex items-center">
            <a href=""  class="font-bold px-2">VH</a>
          </div>

          <!-- Navigation Links -->
          <div class="hidden space-x-8 sm:flex">
            <a href="{{route('voyager.dashboard')}}" title="{{__('Go to admin dasboard')}}"
              class="inline-flex items-center px-1 pt-1 border-b-2 border-transparent hover:border-white text-sm font-medium leading-5 text-gray-200 hover:text-white transition duration-150 ease-in-out">
              {{ __('Dashboard') }}
            </a>
          </div>
        </div>

        <!-- Settings Dropdown -->
        <div class="hidden lg:flex sm:items-center sm:ml-6">
          <div class="text-sm font-medium text-gray-200 hover:text-white hover:border-gray-300 transition duration-150 ease-in-out">
            {{ __('This bar is showing with account') }}
            <a href="{{ route('voyager.dashboard') }}/user/{{$user->id}}" title="{{__('Go to profile')}}" class="font-bold"> {{ $user->name }}</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Responsive Navigation Menu -->
    {{-- <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
      <div class="pt-2 pb-3 space-y-1">
        <x-responsive-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
          {{ __('Dashboard') }}
        </x-responsive-nav-link>
      </div>

      <!-- Responsive Settings Options -->
      <div class="pt-4 pb-1 border-t border-gray-200">
        <div class="px-4">
          <div class="font-medium text-base text-gray-800">{{ $user->name }}</div>
          <div class="font-medium text-sm text-gray-500">{{ $user->email }}</div>
        </div>

        <div class="mt-3 space-y-1">
          <!-- Authentication -->
          <form method="POST" action="{{ route('logout') }}">
            @csrf

            <x-responsive-nav-link :href="route('logout')" onclick="event.preventDefault();
                                          this.closest('form').submit();">
              {{ __('Log Out') }}
            </x-responsive-nav-link>
          </form>
        </div>
      </div>
    </div> --}}
  </nav>
@endif

<!-- Slider main container -->
<div id="homeBannerSwiper" class="swiper w-full">
  <!-- Additional required wrapper -->
  <div class="swiper-wrapper">
    <!-- Slides -->
    @foreach ($photos as $photo)
      {{-- @dd($photo) --}}
      <div class="swiper-slide">
        <div class="relative pb-[41.666666666666664%] min-h-[400px] overflow-hidden">
          <div class="absolute w-full h-full top-0 left-0">
            <img src="{{ Voyager::image($photo->getImage())}}" alt="" class="w-full h-full object-cover" loading="lazy">
          </div>
        </div>
      </div>
    @endforeach
  </div>
  <!-- If we need pagination -->
  <div class="swiper-pagination flex items-center justify-center"></div>

  <!-- If we need navigation buttons -->
  <div class="swiper-button-prev text-5xl text-primary-500">
    <i class='bx bx-chevron-left'></i>
  </div>
  <div class="swiper-button-next text-5xl text-primary-500">
    <i class='bx bx-chevron-right'></i>
  </div>
</div>

<script>
  var timeDelaySwiper = 5000;
  // console.log(document.querySelectorAll('#homeBannerWwiper .swiper-pagination-bullet'));
  // document.querySelector('#homeBannerWwiper .swiper-pagination-bullet::after').style.transition = `all ${timeDelaySwiper/1000}s linear`
  var styleElem = document.head.appendChild(document.createElement("style"));
  const homeBannerSwiper = new Swiper('#homeBannerSwiper', {
    loop: true,
    autoplay: {
      delay: timeDelaySwiper
    },

    pagination: {
      el: '.swiper-pagination',
    },

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    on: {
      init: function () {
        styleElem.innerHTML = `#homeBannerSwiper .swiper-pagination-bullet.swiper-pagination-bullet-active::after {animation: widthToFull ${timeDelaySwiper/1000}s linear;}`;
      },
      autoplayStart: function () {
        styleElem.innerHTML = `#homeBannerSwiper .swiper-pagination-bullet.swiper-pagination-bullet-active::after {animation: widthToFull ${timeDelaySwiper/1000}s linear;}`;
      },
      autoplayStop: function () {
        styleElem.innerHTML = `#homeBannerSwiper .swiper-pagination-bullet.swiper-pagination-bullet-active::after {animation: none }`;
      }
    },
  });

  document.querySelector('#homeBannerSwiper').addEventListener('mouseenter', (e) => {
    homeBannerSwiper.autoplay.stop()
    styleElem.innerHTML = `#homeBannerSwiper .swiper-pagination-bullet.swiper-pagination-bullet-active::after {animation: none }`;
  })
  document.querySelector('#homeBannerSwiper').addEventListener('mouseleave', (e) => {
    homeBannerSwiper.autoplay.start()
    styleElem.innerHTML = `#homeBannerSwiper .swiper-pagination-bullet.swiper-pagination-bullet-active::after {animation: widthToFull ${timeDelaySwiper/1000}s linear;}`;
  })

  document.addEventListener("visibilitychange", function(e) {
    if (document.visibilityState === 'visible') {
      styleElem.innerHTML = `#homeBannerSwiper .swiper-pagination-bullet.swiper-pagination-bullet-active::after {animation: widthToFull ${timeDelaySwiper/1000}s linear;}`;
      console.log('visi');
    } else {
      styleElem.innerHTML = `#homeBannerSwiper .swiper-pagination-bullet.swiper-pagination-bullet-active::after {animation: none }`;
    }
  })
</script>

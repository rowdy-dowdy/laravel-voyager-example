<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Photo extends Model
{
  public function gallery()
  {
    return $this->belongsTo('App\Models\Gallery');
  }

  public function getAll() {
    return $this->with('gallery')->get();
  }

  public function getImage () {
    return $this->image;
  }

  // default select by id gallery
  public function getPhotoByGallery($key = null, $value = null) {
    if ($key == null) {
      return;
    }

    if ($value == null) {
      $value = $key;
      $key = 'id';
    }

    return $this->whereHas('gallery', function ($query) use ($key, $value) {
      return $query->where($key, '=', $value);
    })->get();
  }
}

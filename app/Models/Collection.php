<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Collection extends Model
{
  public function product()
  {
    return $this->belongsToMany('App\Models\Product', 'pivot_product_collection', 'collection_id', 'product_id');
  }

  public function parent()
  {
    return $this->belongsTo('App\Models\Collection', 'parent_id');
  }

  public function childs()
  {
    return $this->hasMany('App\Models\Collection', 'parent_id');
  }

  public function childrens() {
    return $this->childs()->with('childrens');
  }

  public function getAllRoot() {
    return $this->where('parent_id', NULL)->get();
  }

  public function getAllWithChild() {
    return $this->with('childrens')->where('parent_id', NULL)->get();
  }

  public function getImage()
  {
    return $this->image;
  }

  public function getDetail($key = null, $value = null)
  {
    if ($key == null) {
      return;
    }

    if ($value == null) {
      $value = $key;
      $key = 'id';
    }
    return $this->where($key, $value)->get();
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Photo;
use App\Models\Collection;

class HomeController extends Controller
{
  public function index () {

    $photos = (new Photo)->getPhotoByGallery('slug','main-top');
    $collections = (new Collection)->limit(3)->get();

    $test = (new Collection)->getAllWithChild();

    dd($test);

    return view('pages.home',[
      'photos' => $photos,
      'collections' => $collections
    ]);
  }
}
